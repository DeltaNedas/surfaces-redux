#Surfaces Redux
Factorio 0.17 port of Surfaces Remake.
Reach up to the skies or dig deep below, expand your base vertically... If you're into that kind of thing.
Original by Simcra, ported to 0.14-0.16 by Erdbeerbaer. Ported to 0.17 by DeltaNedas.